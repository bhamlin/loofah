#![allow(unused)]

use std::collections::HashMap;
use std::rc::Rc;
use loofah::model::{SoapBody, SoapHeader, SoapMessage, SoapPart};

const NS: &str = "http://a.thing/dot.com";

fn main() {
    let mut message = SoapMessage::default();
    message.set_header(SimpleHeader { user: "A".into(), pwd: "B".into() });
    message.set_body(SimpleBody { content: SimpleContent { text: "C".into() } });

    println!("{message}");
}

struct SimpleHeader {
    user: String,
    pwd: String,
}

struct SimpleBody {
    content: SimpleContent,
}

struct SimpleContent {
    text: String,
}

impl SoapHeader for SimpleHeader {}
impl SoapPart for SimpleHeader {
    fn get_tag(&self) -> &str {
        "SimpleHeader"
    }

    fn get_namespace(&self) -> &str {
        NS
    }

    fn get_attributes(&self) -> Option<HashMap<&str, &str>> {
        let mut map = HashMap::default();
        map.insert("user", self.user.as_str());
        map.insert("pwd", self.pwd.as_str());

        Some(map)
    }
}

impl SoapBody for SimpleBody {}
impl SoapPart for SimpleBody {
    fn get_tag(&self) -> &str {
        "SimpleBody"
    }

    fn get_namespace(&self) -> &str {
        NS
    }

    fn get_children(&self) -> Option<Vec<Rc<&dyn SoapPart>>> {
        Some(vec![Rc::new(&self.content)])
    }
}

impl SoapPart for SimpleContent {
    fn get_tag(&self) -> &str {
        "content"
    }

    fn get_namespace(&self) -> &str {
        NS
    }

    fn get_text(&self) -> Option<&str> {
        Some(self.text.as_str())
    }
}