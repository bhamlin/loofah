use std::collections::HashMap;
use std::fmt::{Display, Formatter};
use std::rc::Rc;
use crate::prelude::NAMESPACE_SOAP;
use crate::soap::render_to_string;

pub trait SoapPart {
    fn get_tag(&self) -> &str;

    fn get_namespace(&self) -> &str;

    fn get_attributes(&self) -> Option<HashMap<&str, &str>> { None }

    fn get_children(&self) -> Option<Vec<Rc<&dyn SoapPart>>> { None }

    fn get_text(&self) -> Option<&str> { None }
}

pub trait SoapHeader: SoapPart {}

pub trait SoapBody: SoapPart {}

pub struct SoapMessage {
    namespace: String,
    header: Option<Rc<dyn SoapHeader>>,
    body: Option<Rc<dyn SoapBody>>,
}

impl SoapPart for SoapMessage {
    fn get_tag(&self) -> &str {
        "Envelope"
    }

    fn get_namespace(&self) -> &str {
        self.namespace.as_str()
    }
}

impl Default for SoapMessage {
    fn default() -> Self {
        Self::new_with_ns(NAMESPACE_SOAP)
    }
}

impl Display for SoapMessage {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", render_to_string(self))
    }
}

impl SoapMessage {
    pub fn new_with_ns(namespace: &str) -> Self {
        Self {
            namespace: namespace.into(),
            header: None,
            body: None,
        }
    }

    pub fn set_header<T: SoapHeader + 'static>(&mut self, header: T) {
        self.header = Some(Rc::new(header));
    }

    pub(crate) fn get_header(&self) -> Option<Rc<dyn SoapHeader>> {
        self.header.clone()
    }

    pub fn set_body<T: SoapBody + 'static>(&mut self, body: T) {
        self.body = Some(Rc::new(body));
    }

    pub(crate) fn get_body(&self) -> Option<Rc<dyn SoapBody>> {
        self.body.clone()
    }
}
