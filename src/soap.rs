use std::collections::HashMap;
use std::rc::Rc;
use elementtree::Element;
use crate::model::{SoapHeader, SoapMessage, SoapPart};

pub(crate) fn render_to_string(soap_message: &SoapMessage) -> String {
    let soap_namespace = soap_message.get_namespace();
    let mut envelope = {
        let tag = soap_message.get_tag();
        Element::new((soap_namespace, tag))
    };
    envelope.set_namespace_prefix(soap_namespace, "soap");

    if let Some(header_entry) = soap_message.get_header() {
        append_child(&mut envelope, soap_namespace, "Header", header_entry);
    }

    if let Some(body_entry) = soap_message.get_body() {
        append_child(&mut envelope, soap_namespace, "Body", body_entry);
    }

    envelope.to_string().unwrap()
}

fn append_child<T: SoapPart + ?Sized>(e: &mut Element, ns: &str, tag: &str, child_entry: Rc<T>) {
    let child_namespace = child_entry.get_namespace();
    let child_tag = child_entry.get_tag();
    let soap_header = e.append_new_child((ns, tag));
    let soap_header = soap_header.append_new_child((child_namespace, child_tag));

    if let Some(attribute_map) = child_entry.get_attributes() {
        apply_attributes(soap_header, child_namespace, attribute_map);
    }

    if let Some(body_text) = child_entry.get_text() {
        soap_header.set_text(body_text);
    } else if let Some(child_list) = child_entry.get_children() {
        apply_children(soap_header, child_namespace, child_list)
    }
}

fn apply_children<T: SoapPart + ?Sized>(e: &mut Element, ns: &str, child_list: Vec<Rc<&T>>) {
    for child in child_list {
        let tag = child.get_tag();
        let c = e.append_new_child((ns, tag));
        c.set_text(child.get_text().unwrap());
    }
}

fn apply_attributes(e: &mut Element, ns: &str, attribute_map: HashMap<&str, &str>) {
    for (key, value) in attribute_map.into_iter() {
        e.set_attr((ns, key), value);
    }
}
